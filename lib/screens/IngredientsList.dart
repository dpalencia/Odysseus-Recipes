import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

// DB Connection Based of of:
// https://codelabs.developers.google.com/codelabs/flutter-firebase/index.html#8

class IngredientsList extends StatelessWidget {
  // The returned streambuilder will listen for updates 
  // to the DB and update itself with live data.
  Widget _buildBody(BuildContext context) {
    return StreamBuilder<QuerySnapshot> (
      // Stream will return List<DocumentSnapshot>
      // Will pass the list of snapshots into the function
      // we specify in the builder
      stream: Firestore.instance.collection('ingredients').snapshots(),
      builder: (context, snapshot) {
        if(!snapshot.hasData) return LinearProgressIndicator();
        return _buildList(context, snapshot.data.documents); 
      },
    );
  }
  
  // _buildBody calls this _buildList with up to date data.
  // This _buildList function returns a ListView--a dynamically
  // generated list of items.
  Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
    return ListView(
      children: snapshot.map((data) => _buildIngredient(context, data)).toList()
    );
  }

  Widget _buildIngredient(BuildContext context, DocumentSnapshot data) {
      final ingredient = Ingredient.fromSnapshot(data);
      return Padding(
        child: Container(
          child: ListTile(
            title: Text(ingredient.name)
          )
        ), 
        padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0)
      );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Ingredients List"),
      ),
      body: _buildBody(context
      )
    );
  }
}


class Ingredient {
  final String name;
  final DocumentReference reference;
  Ingredient.fromMap(Map<String, dynamic> map, {this.reference}) : name = map['name'];

  Ingredient.fromSnapshot(DocumentSnapshot snapshot) 
    : this.fromMap(snapshot.data, reference: snapshot.reference);
  @override 
  String toString() => "Record<$name>";
}